# Matriz del tablero de ajedrez
matriz = []


# funcion que llena la matriz inicial con X
def llenar_matriz():
    # ciclo para llenar las 64 casillas
    for i in range(64):
        # agregación de string
        matriz.append("|XX|")


# función que ejecuta el juego
def juego():
    # solicitud de posición
    x = int(input("Por favor digite posición inicial de juego (numero entre 0 y 63): "))
    # se marca la posicion inicial
    matriz[x] = "|01|"
    # contador que lleva la secuencia de movimiento
    contador = 2
    # variable para mantenerse en el ciclo while
    a = 1

    # ciclo para ejecución de
    while (contador < 65):
        mi_temp = llena_temporal(x)
        y = menor(mi_temp)
        if contador < 10:
            matriz[y] = "|0" + str(contador) + "|"
        else:
            matriz[y] = "|" + str(contador) + "|"
        contador += 1
        x = y



# función que llena una lista con las posibles jugadas y su cantidad posibles movimientos siguientes
def llena_temporal(x):
    # lista donde se almacenarán las posibles jugadas
    temporales = []
    temp1 = mov1(x)  # ------------------- evalua primer movimiento
    # llamada a la función comp65
    comp65(temp1, temporales)
    temp1 = mov2(x)  # ------------------- evalua segundo movimiento
    comp65(temp1, temporales)
    temp1 = mov3(x)  # ------------------- evalua tercer movimiento
    comp65(temp1, temporales)
    temp1 = mov4(x)  # ------------------- evalua cuarto movimiento
    comp65(temp1, temporales)
    temp1 = mov5(x)  # ------------------- evalua quinto movimiento
    comp65(temp1, temporales)
    temp1 = mov6(x)  # ------------------- evalua sexto movimiento
    comp65(temp1, temporales)
    temp1 = mov7(x)  # ------------------- evalua septimo movimiento
    comp65(temp1, temporales)
    temp1 = mov8(x)  # ------------------- evalua octavo movimiento
    comp65(temp1, temporales)
    return temporales


# función que calcula la menor cantidad de posibles movimientos
def menor(t):
    # variable que almacenará la siguiente posición a jugar
    sig = 0
    # variable que captura la candidad menor de movimiento
    m = 8
    # ciclo para evaluar los elementos de la lista
    for i in range(len(t)):
        # evaluación de numero menor
        if (t[i][1] < m):
            # asignación de nuevo menor
            m = t[i][1]
            # asignación de nuevo siguiente
            sig = t[i][0]
    return sig


# función para evaluar la posibilidad de juego
def comp65(t, temp):
    if (t != 65):
        # agregación de posición de posible jugada
        temp.append([t, 0])
        # asignación de cantidad de movimientos posibles
        temp[len(temp) - 1][1] = evalua_temp2(t)


# función que calcula la cantidad de movimientos posibles de una posición
def evalua_temp2(t):
    suma_tem2 = 0
    temp2 = mov1(t)
    if (temp2 != 65):
        suma_tem2 += 1
    temp2 = mov2(t)
    if (temp2 != 65):
        suma_tem2 += 1
    temp2 = mov3(t)
    if (temp2 != 65):
        suma_tem2 += 1
    temp2 = mov4(t)
    if (temp2 != 65):
        suma_tem2 += 1
    temp2 = mov5(t)
    if (temp2 != 65):
        suma_tem2 += 1
    temp2 = mov6(t)
    if (temp2 != 65):
        suma_tem2 += 1
    temp2 = mov7(t)
    if (temp2 != 65):
        suma_tem2 += 1
    temp2 = mov8(t)
    if (temp2 != 65):
        suma_tem2 += 1
    return suma_tem2


# Movimiento 1
def mov1(p):
    sig = p + 6
    if (sig < 0 or sig > 63 or sig % 8 > 5 or matriz[sig] != "|XX|"):
        return 65
    else:
        return sig


# Movimiento 2
def mov2(p):
    sig = p - 17
    if (sig < 0 or sig > 63 or sig % 8 > 6 or matriz[sig] != "|XX|"):
        return 65
    else:
        return sig


# Movimiento 3
def mov3(p):
    sig = p - 15
    if (sig < 0 or sig > 63 or sig % 8 < 1 or matriz[sig] != "|XX|"):
        return 65
    else:
        return sig


# Movimiento 4
def mov4(p):
    sig = p - 6
    if (sig < 0 or sig > 63 or sig % 8 < 2 or matriz[sig] != "|XX|"):
        return 65
    else:
        return sig


# Movimiento 5
def mov5(p):
    sig = p + 15
    if (sig < 0 or sig > 63 or sig % 8 > 6 or matriz[sig] != "|XX|"):
        return 65
    else:
        return sig


# Movimiento 6
def mov6(p):
    sig = p + 17
    if (sig < 0 or sig > 63 or sig % 8 < 1 or matriz[sig] != "|XX|"):
        return 65
    else:
        return sig


# Movimiento 7
def mov7(p):
    sig = p - 10
    if (sig < 0 or sig > 63 or sig % 8 > 5 or matriz[sig] != "|XX|"):
        return 65
    else:
        return sig


# Movimiento 8
def mov8(p):
    sig = p + 10
    if (sig < 0 or sig > 63 or sig % 8 < 2 or matriz[sig] != "|XX|"):
        return 65
    else:
        return sig


# funcion que muestra el tablero
def mostrar_matriz():
    cont = 0
    for i in range(8):
        for j in range(8):
            print(matriz[cont], end=" ")
            cont += 1
        print("")


# llamada a funciones
llenar_matriz()
juego()
mostrar_matriz()