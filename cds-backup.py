print('elige la unidad en la que está almacenada la imformación:')
print('1.MB - 2.GB - 3.TB')

# variable que captura la opcion del usuario
x = int(input('opción: '))

# condicional que evalua si la opción escogida se encuentra en las posibilidades
if(x in [1,3]):
    y = float(input('Digite la cantidad almacenada: '))
    # condicional que evalua si la cantidad está dada en MB
    if(x == 1):
        # se asigna la parte entera de la división mas una unidad para el residuo
        z = (y//700) + 1
        print('son necesarios '+str(int(z))+' CDs')
    # condicional que evalua si la cantidad está dada en GB
    elif(x == 2):
        z = ((y*1024)//700) + 1
        print('son necesarios ' + str(int(z)) + ' CDs')
    elif(x == 3):
        z = ((y * 1048576) // 700) + 1
        print('son necesarios ' + str(int(z)) + ' CDs')
else:
    print('No es un valor valido de ingreso')