x = input("inserte oracion: ")
z = x.split()
y = "".join(z).lower()
a = 0
b = -1
cont = 0

for i in range(len(y)):
    if(y[a]==y[b]):
        cont+=1
    a+=1
    b-=1

if(cont==len(y)):
    print("la oración SI es palindroma")
else:
    if ((cont/len(y))>0.8):
        print("el porcentaje de coincidencia es "+str(cont/len(y)))
    else:
        print("la oración NO es palindroma")