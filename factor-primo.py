'''

Problem 3:

The prime factors of 13195 are 5, 7, 13 and 29.

What is the largest prime factor of the number 600851475143 ?

url = https://projecteuler.net/problem=3

'''



# function thah calcule the largest prime factor, this print the result
def factor(x):

    # initializing variable for largest number
    largest = 0
    # loop that iterates from 1 to parameter value
    for i in range (1, x+1):
        # condicional that evaluates if parameter is divisible by iterator
        if(x%i==0):
            # cahanging the limit of loop
            x = x/i
            # conditional that evaluates the iteratos is a prime number
            if(prime(i)==1):
                # conditional that evaluate if the iterator is greater than variable "largest"
                # sustitucion method
                if (i>largest):
                    # changing the value if the conditional is True
                    largest = i
            # conditional for evaluate when the limit is one
            if x == 1:
                # showing the result
                print (largest)
                break



# function that return 1 if the parameter is a prime number
def prime(x):
    # conditional that evaluates if the paremeter lower than one
    if(x<2):
        # interruption of function and return zero to say that number is not a prime number
        return 0
    # counter
    sum = 0
    # loop that iterate from 2 to half of parameter
    for i in range (2,int(x/2)+1):
        # conditional that evaluate if iterator
        if(x%i==0):
            # counter increment
            sum = sum +1
    # conditional to assess whether the counter increased
    if(sum==0):
        # return zero to say that is a prime number
        return 1
    else:
        # return zero to say that is not a prime number
        return 0

# calling function "factor" with parameter
factor(600851475143)