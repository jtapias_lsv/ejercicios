
lista_agencias = []

class Agencia():
    """docstring for Agencia_de_Turismo"""

    def __init__(self, pcodigo=0, pnombre='agencia', pdireccion='dir', pcondutores=[], ptours=[]):
        # super(Agencia_de_Turismo, self).__init__()
        # self.arg = arg
        self.codigo = pcodigo
        self.nombre = pnombre
        self.direccion = pdireccion
        self.conductores = pcondutores
        self.tours = ptours

    def guardar_agencia(self, agencia):
        self.lista_agencias.append(agencia)

    def __str__(self):
        return " %s: %s" % (str(self.codigo), self.nombre)


class Tour():
    """docstring for Tours"""

    def __init__(self, pcodigo=0, pdestino='Cartagena', pfecha='01-01-2019', pcosto=0,
                 pduracion=0, pcondutor=0, pturistas=[]):
        self.codigo = pcodigo
        self.destino = pdestino
        self.fecha = pfecha
        self.costo = pcosto
        self.duracion = pduracion
        self.condutor = pcondutor
        self.turistas = pturistas

    def __str__(self):
        return " %s: %s" % (str(self.codigo), self.destino)

    def mayor_todos_meses(self, plista_agencia):
        for m in range(1, 13):
            self.mayor_duracion_mes(plista_agencia, m)

    def agrupa_todos_mes(self, plista_agencia):
        for m in range(1, 13):
            print('los tures del mes '+str(m)+ ' son:')
            total = self.agrupa_mes(plista_agencia, m)
            print('en total son '+str(total)+ ' tures')

    def mayor_duracion_mes(self, plista_agencia, pmes):
        mayor = 0
        nombre = 0
        for a in plista_agencia:
            for t in a.tours:
                if int(t.fecha.split('-')[1]) == pmes:
                    if t.duracion > mayor:
                        mayor = t.duracion
                        nombre = t.destino
        print('Mayor duracion en mes '+str(pmes)+' es: ' + str(nombre))

    def agrupa_mes(self, plista_agencia, pmes):
        suma=0
        for a in plista_agencia:
            for t in a.tours:
                if int(t.fecha.split('-')[1]) == pmes:
                    print(t.destino)
                    suma+=1
        return suma

    def agrupar_agencia(self,plista_agencia):
        for a in plista_agencia:
            print('los tures de la agencia '+a.nombre+' son:')
            for t in a.tours:
                print(t.destino)


class Persona():
    """docstring for Persona"""

    def __init__(self, pidentificacion=0, pnombre='nombre', papellido='apellido', psexo='M',
                 pedad=0, pnacionalidad='colombiano'):
        self.identificacion = pidentificacion
        self.nombre = pnombre
        self.apellido = papellido
        self.sexo = psexo
        self.edad = pedad
        self.nacionalidad = pnacionalidad

    def __str__(self):
        return " %s: %s, %s" % (str(self.identificacion), self.apellido, self.nombre)


class Conductor(Persona):
    """docstring for Conductores"""

    def __init__(self, pcodigo, pagencia, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.codigo = pcodigo
        self.agencia = pagencia

    def total_conductores(self,plista_agencia):
        suma=0
        for a in plista_agencia:
            for c in a.conductores:
                suma+=1
        print('existen '+str(suma)+' conductores')


class Turista(Persona):
    """docstring for Turistas"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def agrupa_todos_sexos(self,plista_agencia):
        for s in ['M','F']:
            print('los turistas del sexo ' + s + ' son:')
            self.agrupa_sexo(plista_agencia,s)

    def agrupa_sexo(self,plista_agencia, psexo):
        sx =[]
        for a in plista_agencia:
            for t in a.tours:
                for tu in t.turistas:
                    if tu.sexo == psexo:
                        sx.append(tu.nombre)
        print(list(set(sx)))

    def agrupa_nacionalidad(self,plista_agencia):
        nacio=[]
        for a in plista_agencia:
            for t in a.tours:
                for tu in t.turistas:
                    nacio.append(tu.nacionalidad)
        nacio2= list(set(nacio))
        for n in nacio2:
            nac=[]
            print('los turistas con nacionalidad '+n+' son:')
            for a in plista_agencia:
                for t in a.tours:
                    for tu in t.turistas:
                        if tu.nacionalidad == n:
                            nac.append(tu.nombre)
            print(list(set(nac)))


    def total_turistas(self,plista_agencia):
        turi=[]
        for a in plista_agencia:
            for t in a.tours:
                for tu in t.turistas:
                    turi.append(tu.nombre)

        print('existen '+str(len(list(set(turi))))+' turistas')




cond1 = Conductor(1, 1, 7920624, 'Jose', 'Tapias', 'M', 40, 'gringo')
cond2 = Conductor(2, 1, 273578, 'Mauricio', 'Pacheco', 'M', 20, 'colombiano')
cond3 = Conductor(1, 2, 385933, 'Fernando', 'Garrido', 'M', 39, 'sayayin')

turi1 = Turista(4545454, 'Sandra', 'Gaviria', 'F', 38, 'colombiano')
turi2 = Turista(3434364, 'Ana', 'Valencia', 'F', 31, 'peruano')
turi3 = Turista(2894758, 'Jader', 'Yepez', 'M', 34, 'venezolano')
turi4 = Turista(3156824, 'Alvaro', 'Hernandez', 'M', 22, 'colombiano')


tour1 = Tour(1, 'San Andres', '03-01-2019', 300, 3, 1, [turi1, turi3])
tour2 = Tour(1, 'Curazao', '22-01-2019', 500, 4, 2, [turi2])
tour3 = Tour(2, 'Islas Rosario', '03-02-2019', 250, 4, 1,[turi4])
tour4 = Tour(2, 'Mexico', '06-03-2019', 600, 8, 1, [turi1, turi3])

agencia1 = Agencia(1, 'Dream Travel', 'Bocagrande', [cond1, cond2], [tour1, tour3])
agencia2 = Agencia(2, 'Pretty Cost','Crespo',[cond3],[tour2,tour4])

lista_agencias.append(agencia1)
lista_agencias.append(agencia2)

tour1.mayor_todos_meses(lista_agencias)
tour1.agrupa_todos_mes(lista_agencias)
tour1.agrupar_agencia(lista_agencias)
turi1.agrupa_todos_sexos(lista_agencias)
turi1.agrupa_nacionalidad(lista_agencias)
turi1.total_turistas(lista_agencias)
cond1.total_conductores(lista_agencias)
