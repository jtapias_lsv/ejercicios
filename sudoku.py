# list where store number of sudoku
a=[]

# sudoku to evaluate
a.append([0,0,0,7,9,3,0,0,0])
a.append([0,3,2,0,6,0,0,9,0])
a.append([0,0,0,2,5,0,0,8,0])
a.append([9,0,0,0,0,0,4,0,6])
a.append([1,2,7,0,0,0,8,3,5])
a.append([4,0,6,0,0,0,0,0,9])
a.append([0,9,0,0,2,7,0,0,0])
a.append([0,7,0,0,1,0,9,5,0])
a.append([0,0,0,3,8,9,0,0,0])

# fuction that evaluate if a number is already in a row
def in_row(num,r):
    sum=0
    for i in range(9):
        if(a[r][i]==num):
            sum+=1
    if(sum>0):
        return True
    else:
        return False

# fuction that evaluate if a number is already in a column
def in_column(num,c):
    sum = 0
    for i in range(9):
        if (a[i][c] == num):
            sum += 1
    if (sum > 0):
        return True
    else:
        return False

# fuction that evaluate if a number is already in a square
def in_square(n,r,c):
    if(r%3==0 and c%3==0):
        return (p1p1(n,r,c) or p1p2(n,r,c) or p2p1(n,r,c) or p2p2(n,r,c))
    elif(r%3==0 and c%3==1):
        return (p1m1(n,r,c) or p1p1(n,r,c) or p2m1(n,r,c) or p2p1(n,r,c))
    elif(r%3==0 and c%3==2):
        return (p1m2(n,r,c) or p1m1(n,r,c) or p2m2(n,r,c) or p2m1(n,r,c))
    elif(r%3==1 and c%3==0):
        return (m1p1(n,r,c) or m1p2(n,r,c) or p1p1(n,r,c) or p1p2(n,r,c))
    elif(r%3==1 and c%3==1):
        return (m1m1(n,r,c) or m1p1(n,r,c) or p1m1(n,r,c) or p1p1(n,r,c))
    elif(r%3==1 and c%3==2):
        return (m1m2(n,r,c) or m1m1(n,r,c) or p1m2(n,r,c) or p1m1(n,r,c))
    elif(r%3==2 and c%3==0):
        return (m2p1(n,r,c) or m2p2(n,r,c) or m1p1(n,r,c) or m1p2(n,r,c))
    elif(r%3==2 and c%3==1):
        return (m2m1(n,r,c) or m2p1(n,r,c) or m1m1(n,r,c) or m1p1(n,r,c))
    elif(r%3==2 and c%3==2):
        return (m2m2(n,r,c) or m2m1(n,r,c) or m1m2(n,r,c) or m1m1(n,r,c))



# block of functions that evaluate a number depending position where is it
# these function are called in square function

def p1p1(num,r,c):
    if(num==a[r+1][c+1]):
        return True
    else:
        return False

def p1p2(num,r,c):
    if(num==a[r+1][c+2]):
        return True
    else:
        return False

def p1m1(num,r,c):
    if(num==a[r+1][c-1]):
        return True
    else:
        return False

def p1m2(num,r,c):
    if(num==a[r+1][c-2]):
        return True
    else:
        return False

def p2p1(num,r,c):
    if(num==a[r+2][c+1]):
        return True
    else:
        return False

def p2p2(num,r,c):
    if(num==a[r+2][c+2]):
        return True
    else:
        return False

def p2m1(num,r,c):
    if(num==a[r+2][c-1]):
        return True
    else:
        return False

def p2m2(num,r,c):
    if(num==a[r+2][c-2]):
        return True
    else:
        return False

def m1p1(num,r,c):
    if(num==a[r-1][c+1]):
        return True
    else:
        return False

def m1p2(num,r,c):
    if(num==a[r-1][c+2]):
        return True
    else:
        return False

def m1m1(num,r,c):
    if(num==a[r-1][c-1]):
        return True
    else:
        return False

def m1m2(num,r,c):
    if(num==a[r-1][c-2]):
        return True
    else:
        return False

def m2p1(num,r,c):
    if(num==a[r-2][c+1]):
        return True
    else:
        return False

def m2p2(num,r,c):
    if(num==a[r-2][c+2]):
        return True
    else:
        return False

def m2m1(num,r,c):
    if(num==a[r-2][c-1]):
        return True
    else:
        return False

def m2m2(num,r,c):
    if(num==a[r-2][c-2]):
        return True
    else:
        return False



# function that show sudoku
def show_matrix():
    for i in range(len(a)):
        print(a[i])

# fuction that runs the game
def cicle():
    show_matrix()
    print(count_zero())
    while (count_zero() > 0):
        again()
        evaluate()
        show_matrix()
        print(count_zero())
        print('****************************************')


# function that pick an option of two posibilities
def evaluate():
    for n in range(1, 10):
        for r in range(9):
            tir=0
            for c in range(9):
                if(a[r][c]==0):
                    if (in_square(n,r,c)==False):
                        if(in_row(n, r) == False):
                            if(in_column(n,c)==False):
                                x=r
                                y=c
                                z=n
                                tir += 1
            if(tir==2):
                a[x][y]=z
                return


# function that evaluate number 1 to 9 in all positions
def again():
    n=1
    while(n<10):
        r=0
        while(r<9):
            tir=0
            c=0
            while(c<9):
                if (a[r][c] == 0):
                    if (in_square(n, r, c) == False):
                        if (in_row(n, r) == False):
                            if (in_column(n, c) == False):
                                x = r
                                y = c
                                z = n
                                tir += 1
                c+=1
            if(tir==1):
                a[x][y] = z
                r=0
                n=1
            else:
                r+=1
        n+=1

    m=1
    while(m<10):
        c=0
        while(c<9):
            tic=0
            r=0
            while(r<9):
                if (a[r][c] == 0):
                    if (in_square(m, r, c) == False):
                        if (in_row(m, r) == False):
                            if (in_column(m, c) == False):
                                x = r
                                y = c
                                z = m
                                tic += 1
                r+=1
            if(tic==1):
                a[x][y] = z
                c=0
                m=1
            else:
                c+=1
        m+=1




# fuction that count how many blank (zero in this case) there is in sudoku
def count_zero():
    c = 0
    for i in range(9):
        for j in range(9):
            if(a[i][j]==0):
                c+=1
    return c


# run the game
cicle()