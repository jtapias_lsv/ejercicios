# list to store the information
employees = []

# variable for follow or stop the program
follow = 1

# list for language
language = {'s':['Nombre: ','Tiempo Laborado: ', 'Salarios: ', 'Horas Extras Diurnas: ', 'Horas Nocturnas: ',
                 'Horas Extras Nocturnas: ', 'Horas Extras Diurnas Festivas: ', 'Horas Extras Nocturnas festivas: ',
                 'liquidacion','cesantias','intereses','primas','vacaciones','¿Desea Continuar? s/n ','s','neto'],
            'e':['Name: ', 'Time Worked: ', 'Salaries: ','Morning Overtime: ', 'Night Hours: ',
                 'Nigth Overtime: ', 'Morning Holiday Overtime: ', 'Night Holiday Overtime: ',
                 'settlement','layoff','interests','bonus','vacations', 'continue? y/n ','y','net']}

# catching the option of language
print('Elija el idioma/Select language  s:spanish e:english')
option = (input('option/opcion: '))



# function for catch the information
def capture(l):
    # calling key language dictionary
    name = input(language.get(l)[0])
    time_worked = int(input(language.get(l)[1]))
    salaries = int(input(language.get(l)[2]))
    morning_over_hours = int(input(language.get(l)[3]))
    night_hours = int(input(language.get(l)[4]))
    night_over_hours = int(input(language.get(l)[5]))
    morning_holi_over_hours = int(input(language.get(l)[6]))
    night_holi_over_hours = int(input(language.get(l)[7]))

    information = [name,time_worked,salaries,morning_over_hours,night_hours,night_over_hours,
                   morning_holi_over_hours, night_holi_over_hours]

    return information


def salary(sal):
    '''
            function to calculate value of salary - salario
            parameters:  quantity of minimun salaries(sal)
        '''
    return (round((sal * 850000),2))

def layoff(sal, t_w):
    '''
        function to calculate value of layoff - cesantias
        parameters:  quantity of minimun salaries(sal), time_worked(d_w)
    '''
    return (round((((sal*850000)*t_w)/360),2))


def interest_layoff(loff,t_w,):
    '''
        function to calculate value of layoff's interests - intereses de cesantia
        parameters: layoff(loff), time_worded(t_w)
    '''
    return(round(((loff*t_w)/360),2))


def work_bonus(sal,t_w):
    '''
        function to calculate value of work bounus - prima
        parameters: quantity of minimun salaries(sal), time_worked(t_w)
    '''
    return (round((((sal*850000)*(t_w/180))/360),2))


def vacations(t_w):
    '''
        function to calculate value of vacations time- vacaciones
        parameters: time_worked(t_w)
    '''
    return (round(((8500000*t_w)/720),2))


def mornig_over(sal):
    '''
        function to calculate value of morning over time- hora extra diurna
        pameters: quantity of minimun salaries(sal)
    '''
    return (round(((850000*sal*1.25)/240),2))


def nigth(sal):
    '''
        function to calculate value of night time - hora nocturna
        pameters: quantity of minimun salaries(sal)
    '''
    return (round(((850000*sal*1.35)/240),2))


def nigth_over(sal):
    '''
        function to calculate value of night over time - hora extra nocturna
        pameters: quantity of minimun salaries(sal)
    '''
    return (round(((850000*sal*1.75)/240),2))


def morning_holi(sal):
    '''
        function to calculate value of morning time - hora festiva diurna
        pameters: quantity of minimun salaries(sal)
    '''
    return (round(((850000*sal*1.75)/240),2))


def morning_holi_over(sal):
    '''
        function to calculate value of morning over time - hora extra festiva diurna
        pameters: quantity of minimun salaries(sal)
    '''
    return (round(((850000*sal*2)/240),2))



def night_holi_over(sal):
    '''
        function to calculate value of night over time - hora extra festiva nocturna
        pameters: quantity of minimun salaries(sal)
    '''
    return (round(((850000*sal*2.5)/240),2))


def execute(f, o):
    # loop  to catch the information of a employee
    while (f == 1):
        # restarting sum
        sum = 0
        # restarting info
        info = []
        # calling fuction capture
        info = capture(o)
        # restarting dictionary
        dic = {}
        # including information
        dic.update({language.get(o)[0]:info[0]})
        dic.update({'SM': salary(info[2])})
        dic.update({'HED': [info[3], mornig_over(info[2])]})
        dic.update({'HN': [info[4], nigth(info[2])]})
        dic.update({'HEN': [info[5], nigth_over(info[2])]})
        dic.update({'HEDF': [info[6], morning_holi_over(info[2])]})
        dic.update({'HENF': [info[7], morning_holi_over(info[2])]})

        dic.update({language.get(o)[8]:{language.get(o)[9]:layoff(info[2], info[2]),
                                   language.get(o)[10]:interest_layoff(layoff(info[2], info[2]),info[1]),
                                   language.get(o)[12]:vacations(info[1]),
                                   language.get(o)[11]:work_bonus(info[2],info[1])
                                   }})

        # calculating liquidation
        sum += salary(info[2]) + layoff(info[2], info[2]) + interest_layoff(layoff(info[2], info[2]),info[1]) + \
              vacations(info[1]) + work_bonus(info[2],info[1])

        # adding liquidation
        dic.update({language.get(o)[15]:sum})

        # includding new record
        employees.append(dic)

        # asking user if want to continue
        qf = input(language.get(o)[13]).lower()
        if (qf == 's' or qf == 'y'):
            f = 1
        else:
            f = 0

    # returning  sum of all
    return sum


# calling function execute to start to get the information
total = execute(follow,option)

# showing all employees
for employee in employees:
    print(employee)

# showing total pay for company
print('TOTAL: '+str(total))



