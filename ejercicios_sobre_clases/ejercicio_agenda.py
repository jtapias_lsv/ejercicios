class Agenda:
    '''Class to get the contacts'''

    listado_contacto = []

    def guardar_contacto(self, p_contacto):
        self.listado_contacto.append(p_contacto)

    def listar_contactos(self):
        print('\nIMPRIMIENDO CONTACTOS:')
        print('-----------------------------')
        for contacto in self.listado_contacto:
            contacto.imprimir_contacto()
            print('****************')

    def editar_contacto(self, p_contacto, p_nombre, p_email, p_tele):
        indice = self.listado_contacto.index(p_contacto)
        cont_temporal = Contacto(p_nombre,p_email, p_tele)
        self.listado_contacto[indice] =  cont_temporal




    def eliminar_contacto(self, p_contacto):
        self.listado_contacto.remove(p_contacto)

class Contacto:

    def __init__(self, p_nombre, p_email, p_telefono):
        self.nombre = p_nombre
        self.email = p_email
        self.telefono = p_telefono


    def imprimir_contacto(self):
        data = f"Nombre {self.nombre}\nEmail: {self.email}\nTelefono: {self.telefono}"
        print(data)


# calling methods
my_contact1 = Contacto('Jose','jose@gmail.com',123)
my_contact1.imprimir_contacto()
my_contact2 = Contacto('Ana','ana@gmail.com',456)
my_contact3 = Contacto('Mauricio','elfloww@gmail.com',789)

my_agenda_google = Agenda()
my_agenda_google.guardar_contacto(my_contact1)
my_agenda_google.guardar_contacto(my_contact2)
my_agenda_google.listar_contactos()
my_agenda_google.eliminar_contacto(my_contact1)
my_agenda_google.listar_contactos()
my_agenda_facebook = Agenda()
my_agenda_facebook.guardar_contacto(my_contact3)
my_agenda_facebook.editar_contacto(my_contact2,'Juana','juana@gmail.com',987)
my_agenda_facebook.listar_contactos()