# Diccionario donde serán guardados los contactos
myDicc = {}
# Variable que determina la continuación del programa
sigue = 's'

# ciclo que itera la solicitud de datos
while (sigue == 's'):
    x = input('digite nombre: ').lower()
    y = input('digite telefono: ').lower()

    # condicional que evaluas la existencia de la clave del contacto
    if(myDicc.get(x)== None):
        # si elcondicional es True se agrega el contacto
        myDicc.update({x: y})
        print('****Usuario Guardado*****')
    else:
        # si el condicional es False pregunta si desea actualizarlo o no
        d = input(('Contacto ya existe Desea actualizarlo? s/n')).lower()

        # condicional que evalua si el usuario desea actualizar el contacto
        if(d=='s'):
            # si el condicional es True se actualiza el contacto
            myDicc.update({x: y})
            print('****Usuario Actualizado*****')

    sigue = input('Desea continuar s/n: ').lower()

# impresion de contactos
print('Existen '+str(len(myDicc))+' contactos')
print(myDicc)
