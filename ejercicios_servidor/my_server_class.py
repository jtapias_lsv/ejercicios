'''
source: https://www.acmesystems.it/python_http
I make some changes
'''

#!/usr/bin/python
import http.server
import socketserver
# modul to get special caracteres
from os import curdir, sep

# defining port
PORT = 8080


# This class will handles any incoming request from
# the browser
class myHandler(http.server.SimpleHTTPRequestHandler):

    # Handler for the GET requests
    def do_GET(self):
        if self.path == "/":
            self.path = "/index.html"

        try:
            # Check the file extension required and
            # set the right mime type

            # enabling html files
            sendReply = False
            if self.path.endswith(".html"):
                mimetype = 'text/html'
                sendReply = True

            # enabling css files
            if self.path.endswith(".css"):
                mimetype = 'text/css'
                sendReply = True

            # rendering file in browser
            if sendReply == True:
                # Open the static file requested and send it mode byte
                f = open(curdir + sep + self.path,'rb')
                self.send_response(200)
                self.send_header('Content-type', mimetype)
                self.end_headers()
                self.wfile.write(f.read())
                f.close()
            return

        # to catch if the file in path dosen't exist
        except IOError:
            f = open(curdir + sep + "/notfound.html", 'rb')
            self.send_response(200)
            self.send_header('Content-type', mimetype)
            self.end_headers()
            self.wfile.write(f.read())
            f.close()
            # if want to send 404 page
            #self.send_error(404, 'File is Not Found: %s' % self.path)


try:
    # Create a web server and define the handler to manage the
    # incoming request
    server = socketserver.TCPServer(('', PORT), myHandler)
    print('Started httpserver on port ', PORT)

    # Wait forever for http requests
    server.serve_forever()

except KeyboardInterrupt:
    # to interrupt the execute of server
    print('^C received, shutting down the web server')
    server.socket.close()