import requests


def users():
    nombres = []
    url = "https://jsonplaceholder.typicode.com/users"
    response = requests.get(url)
    if response.status_code == 200:
        results = response.json()
        # print(results) # results es una lista de dicionarios
        for result in results:
            nombres.append(result)
    else:
        print("Error code %s" % response.status_code)

    return nombres

def photos():
    list_fotos = []
    url = "https://jsonplaceholder.typicode.com/photos"
    response = requests.get(url)
    if response.status_code == 200:
        results = response.json()
        # print(results) # results es una lista de dicionarios
        for result in results:
            list_fotos.append(result)
    else:
        print("Error code %s" % response.status_code)

    return list_fotos


file = open("index.html", "w")
encabezado='''<!DOCTYPE html>
<html><head>
<title>LSV Team</title>
 <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<div class="container">
'''
footer='''</body>
</html>
'''

cards_photo_ini='''
<div class="row">
  <h2>Photos</h2>          
  <div class="card-deck">
    '''
cards_photo_midd=''''''
for p in photos()[0:16]:
    cards_photo_midd += '''
    <div class="col-xs-6 col-sm-3">
      <div class="card bg-primary">
        <img class="card-img-top" src=" '''+p.get('thumbnailUrl')+''''" alt="Card image" ">
        <br>
      </div>
    </div>
      '''

cards_photho_end='''
  </div>
</div>
'''




table_user_ini='''

  <h2>Users</h2>          
  <table class="table table-dark table-hover">
    <thead>
      <tr>
        <th>Id</th>
        <th>Username</th>
        <th>Name</th>
        <th>Email</th>
      </tr>
    </thead>
    <tbody>
    '''
table_user_midd=''''''
for n in users():
    table_user_midd += '''
      <tr>
        <td>'''+str(n.get('id'))+'''</td>
        <td>'''+n.get('username')+'''</td>
        <td>'''+n.get('name')+'''</td>
        <td>'''+n.get('email')+'''</td>
      </tr>
      '''

table_user_end='''</tbody>
  </table>
</div>
'''

file.write(encabezado)
file.write(cards_photo_ini)
file.write(cards_photo_midd)
file.write(cards_photho_end)
file.write(table_user_ini)
file.write(table_user_midd)
file.write(table_user_end)
file.write(footer)
file.close()