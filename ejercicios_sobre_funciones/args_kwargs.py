agenda=[]

def guardar_contacto(id, nombre, telefono, **kwargs):
    contact = {'id': id, 'nombre': nombre, 'telefono': telefono}
    contact.update({'home_tel':kwargs.get('home_tel')})
    contact.update({'dir': kwargs.get('dir')})
    contact.update({'whatsapp': kwargs.get('whatsapp')})
    contact.update({'telegram': kwargs.get('telegram')})
    agenda.append(contact)

def listar_contactos(*args):
    for contacto in agenda:
        if contacto.get('dir') == None:
            contacto.update({'dir':'Vacio'})
        if contacto.get('whatsapp') == None:
            contacto.update({'whatsapp':'Vacio'})
        if contacto.get('telegram') == None:
            contacto.update({'telegram':'Vacio'})
        mensaje = 'Id: {id} \nNombre: {nombre} \nTelefono: {home_tel} \nDir: {dir} \n' \
                  "WhatsApp: {whatsapp} \nTelegram: {telegram}".format(**contacto)
        print('------------------------------------------')
        print(mensaje)


guardar_contacto(10, 'Jose', 313999999, home_tel=6768525, dir='Jardines', whatsapp=True, telegram=False)
guardar_contacto(20, 'Sandra', 3100000, home_tel=6768525, telegram=True)
listar_contactos(agenda)