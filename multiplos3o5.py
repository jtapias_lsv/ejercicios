'''
Problem 1:

If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9.
The sum of these multiples is 23.

Find the sum of all the multiples of 3 or 5 below 1000.

url= https://projecteuler.net/problem=1
'''

# variable that store the sum
sum = 0

# loop that iterates the numbers from 1 to 1000
for i in range(1000):
    # conditional that evaluates if the number is divisible by 3 or 5
    if(i%3==0 or i%5==0):
        # adding number to the variable
        sum +=i

# showing result
print(sum)